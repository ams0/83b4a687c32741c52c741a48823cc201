#AKS for storage

az aks create -k 1.19.3 -g k8s  -s Standard_B4ms -c 1 -n storage
az aks nodepool add -g k8s --cluster-name storage --name rook  -c 0 -s Standard_B4ms --labels storage=rook
az vmss disk attach --vmss-name aks-rook-00821560-vmss -g MC_k8s_storage_westeurope --sku StandardSSD_LRS -z 60 --lun 0
az vmss disk attach --vmss-name aks-rook-00821560-vmss -g MC_k8s_storage_westeurope --sku StandardSSD_LRS -z 60 --lun 1
az aks nodepool scale -g k8s --cluster-name storage --node-count 3 -n rook